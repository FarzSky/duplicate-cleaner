import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDateDto {

    public String year;
    public String month;
    public String day;
    public long millis;

    public FileDateDto(long millis) {
        this.millis = millis;
        this.setDate(this.millis);
    }

    public void setDate(long millis) {

        Date date = new Date(millis);
        SimpleDateFormat sdDay = new SimpleDateFormat("dd");
        SimpleDateFormat sdMonth = new SimpleDateFormat("MM");
        SimpleDateFormat sdYear = new SimpleDateFormat("yyyy");

        this.day = sdDay.format(date);
        this.month = sdMonth.format(date);
        this.year = sdYear.format(date);
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
