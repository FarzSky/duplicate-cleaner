import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class DuplicateCleaner {

    private final String originals = "originals";
    private final String processed = "processed";

    private Path rootFolderPath;
    private Path resultPath;

    private Stack<Path> files = new Stack<>();
    private Map<String, Integer> fileMap = new HashMap<>();
    private BufferedWriter writer;
    private FileUtil fileUtil;

    public DuplicateCleaner(String rootFolderPath, String resultFolderPath) {
        this.resultPath = Paths.get(resultFolderPath, "DuplicateCleaner");
        this.rootFolderPath = Paths.get(rootFolderPath);
        System.out.println("Root path: " + this.rootFolderPath);
        System.out.println("Result path: " + this.resultPath);

        this.fileUtil = new FileUtil();
    }

    public void run() throws IOException {

        // prepare stuff
        this.fileMap = new HashMap<>();
        this.fileUtil.createFolders(this.resultPath);

        Path statusFilePath = Paths.get(this.resultPath.toString(), "status.txt");
        Path exceptionsFilePath = Paths.get(this.resultPath.toString(), "exceptions.txt");

        statusFilePath.toFile().createNewFile();
        exceptionsFilePath.toFile().createNewFile();

        this.writer = new BufferedWriter(new FileWriter(statusFilePath.toString()));
        BufferedWriter exceptionBuffer = new BufferedWriter(new FileWriter(exceptionsFilePath.toString()));

        // make stack of all files in root
        Files.find(this.rootFolderPath, 999, (p, bfa) -> bfa.isRegularFile())
                .forEach(c -> this.files.push(c));

        // initiate status
        long totalFiles = this.files.size();
        long currentStatus = 0;
        int lastPercantage = -1;

        // iterate over stack
        while (!this.files.empty()) {

            try {
                runSingle();
            } catch (Exception e) {
                System.out.println("Exception: " + e.getMessage());

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);

                exceptionBuffer.newLine();
                exceptionBuffer.newLine();
                exceptionBuffer.write(sw.toString());
            } finally {
                currentStatus += 1;
                int newPercantage = this.fileUtil.percantage(currentStatus, totalFiles);
                if (newPercantage > lastPercantage) {
                    System.out.println("Currently processed: " + newPercantage + "%");
                    lastPercantage = newPercantage;
                }
            }

        }

        this.writer.close();
        exceptionBuffer.close();
    }

    private void runSingle() throws IOException {

        Path filePath = this.files.pop();

        String fileName = filePath.getFileName().toString();
        String fileType = this.fileUtil.getFileType(fileName);
        Path fileFolder = filePath.getParent();

        BasicFileAttributes fileAttrs = Files.readAttributes(filePath, BasicFileAttributes.class);
        FileDateDto fileModifiedDate = new FileDateDto(fileAttrs.lastModifiedTime().toMillis());

        // if first then put in dict and copy to original folder
        if (!this.fileMap.containsKey(fileName)) {

            Path originalFolder = Paths.get(this.resultPath.toString(), this.originals, fileType, fileModifiedDate.year, fileModifiedDate.month, fileModifiedDate.day);
            this.fileUtil.createFolders(originalFolder);
            Path originalFile = Paths.get(originalFolder.toString(), fileName);
            Files.copy(filePath, originalFile);
            this.fileMap.put(fileName, 0);

            this.writer.newLine();
            this.writer.write(filePath + " ->  " + originalFile);

        // if dict contains filename, copy to duplicates folder
        } else {

            Path duplicatesFolder = Paths.get(this.resultPath.toString(), this.originals, fileType,
                    fileModifiedDate.year, fileModifiedDate.month, fileModifiedDate.day, this.fileUtil.getFilename(fileName));
            this.fileUtil.createFolders(duplicatesFolder);

            // if file exist in duplicate, rename to filename_(copy_number)
            Path duplicateFile = Paths.get(duplicatesFolder.toString(), fileName);
            if (Files.exists(duplicateFile)) {
                duplicateFile = this.fileUtil.addCopyNumber(duplicateFile, this.fileMap.get(fileName));
            }
            Files.copy(filePath, duplicateFile);
            this.fileMap.replace(fileName, this.fileMap.get(fileName) + 1);

            this.writer.newLine();
            this.writer.write(filePath + " ->  " + duplicateFile);
        }

        // move original to folder processed -> original folder structure
        Path processedFolder = Paths.get(this.resultPath.toString(), this.processed,
                Paths.get(fileFolder.toString().replace(this.rootFolderPath.toString(), "")).toString());
        this.fileUtil.createFolders(processedFolder);
        Path processedFile = Paths.get(processedFolder.toString(), fileName);
        Files.move(filePath, processedFile);

        this.writer.newLine();
        this.writer.write(filePath + " ->  " + processedFile);
    }
}
