import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        if (args == null || args.length < 2) {
            throw new NullPointerException("Please provide the starting full path and result path.");
        }

        CsvDuplicateCleaner csvDuplicateCleaner = new CsvDuplicateCleaner(args[0], args[1]);

        try {
            csvDuplicateCleaner.run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
