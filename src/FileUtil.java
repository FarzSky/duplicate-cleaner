import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class FileUtil {

    public FileUtil() {
    }

    public void createFolders(Path folderPath) {
        try {
            Files.createDirectories(folderPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Path addCopyNumber(Path filePath, int number) {
        String fullName = filePath.getFileName().toString();
        int extIndex = fullName.lastIndexOf(".");

        String fileName = fullName.substring(0, extIndex) + " - copy (" + number + ")";
        if (extIndex < fullName.length() - 1) {
            fileName += fullName.substring(extIndex);
        }

        return Paths.get(filePath.getParent().toString(), fileName);
    }

    public int percantage(long currentStatus, long totalFiles) {
        return (int) (((float) currentStatus / totalFiles) * 100);
    }

    public String getFileType(String fileName) {
        String result = FILE_TYPE_MAP.get("other");
        if (fileName == null) return result;

        String[] exts = fileName.split("\\.");
        String ext = "." + exts[exts.length - 1].toLowerCase();

        if (FILE_TYPE_MAP.containsKey(ext)) {
            result = FILE_TYPE_MAP.get(ext);
        }

        return result;
    }

    public String getFilename(String fileName) {
        if (fileName == null) return "unknown";

        String[] parts = fileName.split("\\.");
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<parts.length - 1; i++) {
            if (i == 0) {
                sb.append(parts[i]);
            } else {
                sb.append("." + parts[i]);
            }
        }

        return sb.toString();
    }

    public final Map<String, String> FILE_TYPE_MAP = new HashMap<>() {
        {
            put(".png", "slike");
            put(".jpeg", "slike");
            put(".jpg", "slike");
            put(".tiff", "slike");
            put(".gif", "slike");
            put(".bmp", "slike");
            put(".webp", "slike");
            put(".svg", "slike");

            put(".webm", "video");
            put(".mkv", "video");
            put(".flv", "video");
            put(".avi", "video");
            put(".mov", "video");
            put(".wmv", "video");
            put(".amv", "video");
            put(".mp4", "video");
            put(".m4p", "video");
            put(".m4v", "video");
            put(".mpg", "video");
            put(".mpeg", "video");
            put(".mpv", "video");
            put(".3gp", "video");

            put(".mp3", "glazba");
            put(".raw", "glazba");
            put(".wav", "glazba");
            put(".wma", "glazba");
            put(".webm", "glazba");

            put("other", "dokumenti");
        }
    };

}
