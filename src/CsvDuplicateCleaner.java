import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class CsvDuplicateCleaner {

    private Path sourceFolderPath;
    private Path destinationFilePath;

    private Stack<Path> files = new Stack<>();
    private Set<String> numbers = new HashSet<>();
    
    private BufferedWriter writer;
    private FileUtile fileUtil;

    public CsvDuplicateCleaner(String sourceFolderPath, String destinationFilePath) {
        this.sourceFolderPath = Paths.get(sourceFolderPath);
        this.destinationFilePath = Paths.get(destinationFilePath);
        System.out.println("Source path: " + this.rootFolderPath);
        System.out.println("Destination path: " + this.resultPath);

        this.fileUtil = new FileUtil();
    }

    public void run() {

        // prepare stuff
        this.writer = new BufferedWriter(new FileWriter(this.destinationFilePath.toString()));

        // make stack of all files in root
        Files.find(this.sourceFolderPath, 999, (p, bfa) -> bfa.isRegularFile())
                .forEach(c -> this.files.push(c));

        // initiate status
        long totalFiles = this.files.size();
        long currentStatus = 0;
        int lastPercantage = -1;

        // iterate over stack
        while (!this.files.empty()) {
            try {
                runSingle();
            } catch (Exception e) {
                System.out.println("Exception: " + e.getMessage());
            } finally {
                currentStatus += 1;
                int newPercantage = this.fileUtil.percantage(currentStatus, totalFiles);
                if (newPercantage > lastPercantage) {
                    System.out.println("Currently processed: " + newPercantage + "%");
                    lastPercantage = newPercantage;
                }
            }
        }

        this.writer.close();
    }

    private void runSingle() throws IOException {

        // file phone page file
        Path filePath = this.files.pop();

        // convert sheet to csv and append into csv
        InputStream is = new FileInputStream(filePath.toString());
        this.convertXlsxToCSV();
    }

    public void convertXlsxToCSV(InputStream inputStream) throws IOException, InvalidFormatException {
        Workbook wb = WorkbookFactory.create(inputStream);
        csvConverter(wb.getSheetAt(0));
    }

    private void csvConverter(Sheet sheet) {
        Row row = null;
        String str = new String();
        
        // iterate xlsx rows
        for (int i = 0; i < sheet.getLastRowNum() + 1; i++) {
            row = sheet.getRow(i);
            String rowString = new String();
            // skip the header row
            if (row.getCell(0) != null && row.getCell(0).Equals("Prezime")) continue;

            // skip row with existing number
            String phoneNumber = row.getCell(4);
            if (phoneNumber != null)
                if (this.numbers.contains(phoneNumber)) {
                    continue;
                } else {
                    this.numbers.add(phoneNumber);
                }
            }
            
            // iterate xlsx row columns
            for (int j = 0; j < 5; j++) {
                if(row.getCell(j) == null) {
                    rowString += Utility.SEMICOMMA;
                } else {
                    rowString += row.getCell(j) + Utility.SEMICOMMA;
                }
            }

            // remove last semi-comma
            String str = rowString.substring(0, rowString.length() - 1);

            // apend sheet row into csv file
            this.writer.write(str);
            this.writer.newLine();
        }
    }
}
